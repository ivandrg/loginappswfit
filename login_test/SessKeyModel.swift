//
//  SessKeyModel.swift
//  login_test
//
//  Created by Ivan Gustin on 4/06/24.
//

import Foundation

struct SessKeyModel: Codable {
    let id: String?
    let sesskey: String?
}
