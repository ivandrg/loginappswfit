//
//  credentials.swift
//  login_test
//
//  Created by Ivan Gustin on 31/05/24.
//

import Foundation

struct Credentials: Codable {
    let username: String
    let password: String
}

struct AuthResponse: Codable {
    let success: Bool
    let token: String?
}
