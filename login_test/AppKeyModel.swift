//
//  CreateAppKey.swift
//  login_test
//
//  Created by Ivan Gustin on 3/06/24.
//

import Foundation

struct AppKeyModel: Codable, Identifiable  {
    let id: String
    let appkey: String
}
