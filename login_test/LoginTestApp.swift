//
//  login_testApp.swift
//  login_test
//
//  Created by Ivan Gustin on 31/05/24.
//

import SwiftUI

@main
struct LoginTestApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView(isAuthenticated: false, token: "")
        }
    }
}
