// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let booksModel = try? JSONDecoder().decode(BooksModel.self, from: jsonData)

import Foundation

// MARK: - BooksModel
struct BooksModel: Codable {
    let status: String
    let sstamp: Int
    let allBooks: AllBooks
    let createdVNB, req: String
}

// MARK: - AllBooks
struct AllBooks: Codable {
    let nbBooks, nbContacts: Int
    let contacts: [Contact]
    let books: [Book]
}

// MARK: - Book
struct Book: Codable {
    let invited, accepted, archived, showFPOnOpen: Bool
    let sstamp: Int
    let del: Bool
    let hideMessage, hideBookMembers: String
    let description: JSONNull?
    let defaultTemplate: String
    let isDownloadable, canDisableSync: Bool
    let bC: String
    let bO: BO
    let cluster: String
    let tags, langs: JSONNull?
    let contactUC: BO?
    let nbNotRead, nbMembers: Int
    let members: [Member]
    let fpForm: FPForm
    let lastMsg: LastMsg
    let nbMsgs: Int
    let userPrefs: UserPrefs
    let ownerPrefs: OwnerPrefs
    let sbid, lastMsgRead, lastMedia: Int
    let favorite: Bool
    let order: Int
    let contactConfirmed: Bool?

    enum CodingKeys: String, CodingKey {
        case invited, accepted, archived
        case showFPOnOpen = "showFpOnOpen"
        case sstamp, del, hideMessage, hideBookMembers, description, defaultTemplate, isDownloadable, canDisableSync
        case bC = "b_c"
        case bO = "b_o"
        case cluster, tags, langs
        case contactUC = "contact_u_c"
        case nbNotRead, nbMembers, members, fpForm, lastMsg, nbMsgs, userPrefs, ownerPrefs, sbid, lastMsgRead, lastMedia, favorite, order, contactConfirmed
    }
}

enum BO: String, Codable {
    case demo = "demo"
    case mkuil = "mkuil"
    case uu = "uu"
}

// MARK: - FPForm
struct FPForm: Codable {
    let fpid: Int
    let name: String
    let lastModified: Int
}

// MARK: - LastMsg
struct LastMsg: Codable {
    let smid: Int
    let uuid: String
    let sstamp, lastCommentID: Int
    let msgBody: String
    let msgType: MsgType
    let msgMethod, msgColor: String
    let nbComments, pid, nbMedias, nbEmailCids: Int
    let nbDocs: Int
    let bC: String
    let bO: BO
    let uC: String
    let linkedRowID, linkedTabID, linkMessage, linkedFieldID: String?
    let msg: String
    let del: Bool
    let created, lastModified: Int
    let docs, medias: [Doc]?

    enum CodingKeys: String, CodingKey {
        case smid, uuid, sstamp
        case lastCommentID = "lastCommentId"
        case msgBody, msgType, msgMethod, msgColor, nbComments, pid, nbMedias, nbEmailCids, nbDocs
        case bC = "b_c"
        case bO = "b_o"
        case uC = "u_c"
        case linkedRowID = "linkedRowId"
        case linkedTabID = "linkedTabId"
        case linkMessage
        case linkedFieldID = "linkedFieldId"
        case msg, del, created, lastModified, docs, medias
    }
}

// MARK: - Doc
struct Doc: Codable {
    let id: Int
    let ext, originName, internName, uuid: String
    let size: Int
    let type: String
    let del: Bool
    let emailCid: String?
}

enum MsgType: String, Codable {
    case m = "m"
}

// MARK: - Member
struct Member: Codable {
    let uC: UC
    let invite: Invite
    let memberRight, access: Int
    let hideMessage, hideBookMembers, apiRight: String

    enum CodingKeys: String, CodingKey {
        case uC = "u_c"
        case invite
        case memberRight = "right"
        case access, hideMessage, hideBookMembers, apiRight
    }
}

enum Invite: String, Codable {
    case accepted = "accepted"
}

enum UC: String, Codable {
    case demo = "demo"
    case marcb = "marcb"
    case mkuil = "mkuil"
}

// MARK: - OwnerPrefs
struct OwnerPrefs: Codable {
    let fpAutoExport: Bool
    let oCoverColor: String
    let oCoverUseLastImg: Bool
    let oCoverImg: String?
    let oCoverType: CoverType
    let authorizeMemberBroadcast, acceptExternalMsg: Bool
    let title: String
    let notifyMobileConfidential: Bool
}

enum CoverType: String, Codable {
    case color = "color"
    case image = "image"
}

// MARK: - UserPrefs
struct UserPrefs: Codable {
    let maxMsgsOffline: Int
    let syncWithHubic, uCoverLetOwnerDecide: Bool
    let uCoverColor: String
    let uCoverUseLastImg: Bool
    let uCoverImg: String?
    let uCoverType: CoverType
    let inGlobalSearch, inGlobalTasks, notifyEmailCopy, notifySMSCopy: Bool
    let notifyMobile, notifyWhenMsgInArchivedBook: Bool

    enum CodingKeys: String, CodingKey {
        case maxMsgsOffline, syncWithHubic, uCoverLetOwnerDecide, uCoverColor, uCoverUseLastImg, uCoverImg, uCoverType, inGlobalSearch, inGlobalTasks, notifyEmailCopy
        case notifySMSCopy = "notifySmsCopy"
        case notifyMobile, notifyWhenMsgInArchivedBook
    }
}

// MARK: - Contact
struct Contact: Codable {
    let uC, lastName, firstName: String
    let sstamp: Int
    let isConfirmed: Bool

    enum CodingKeys: String, CodingKey {
        case uC = "u_c"
        case lastName, firstName, sstamp, isConfirmed
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
            return true
    }

    public var hashValue: Int {
            return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            if !container.decodeNil() {
                    throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
            }
    }

    public func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            try container.encodeNil()
    }
}
