//
//  ContentView.swift
//  login_test
//
//  Created by Ivan Gustin on 31/05/24.
//

import SwiftUI

struct LoginView: View {
    @State var isAuthenticated: Bool = false
    @State private var username: String = "demo"
    @State private var password: String = "test"
    @State private var loginFailed: Bool = false
    @State var appKey: AppKeyModel?
    @State var oAuthKey: OAuthKeyModel?
    @State var sessKey: SessKeyModel?
    @State var token: String
    
    var body: some View {
        NavigationStack {
        VStack {
            TextField("Username", text: $username)
                .padding()
                .background(Color(.white))
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            
            SecureField("Password", text: $password)
                .padding()
                .background(Color(.white))
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            
            Button(
                action: {
                executeLogin()
            }) {
                Text("Login")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 220, height: 60)
                    .background(Color.blue)
                    .cornerRadius(15.0)
            }
            
            if loginFailed {
                Text("Login Failed. Please try again.")
                    .foregroundColor(.red)
                    .padding(.top, 20)
            }
        }
        .padding().navigationBarTitle("Login")
        .onAppear {
            let token = UserDefaults.standard.value(forKey: "token")
            if token != nil {
                isAuthenticated = true
            }
            if isAuthenticated {
                // Navigate to LandingPageView if isAuthenticated is already true
                navigateToLandingPage()
            }
        }
        .background(Color.gray.opacity(0.1))
        }
    }
    
    func navigateToLandingPage() {
            // Navigate to LandingPageView
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
                if let window = windowScene.windows.first {
                    window.rootViewController = UIHostingController(rootView: LandingPageView())
                    window.makeKeyAndVisible()
                }
            }
        }
        
    func executeLogin() {
        AuthApi().createAppKey { (appKey) in
            self.appKey = appKey
            AuthApi().createOauthkey(appKey: appKey.appkey, email: username, pwd: password ) { (oAuthKey) in
                self.oAuthKey = oAuthKey
                AuthApi().createSessKey(oAuthKey: oAuthKey.oauthkey) {
                    (sessKey) in
                    self.sessKey = sessKey
                    if(sessKey.sesskey != nil) {
                        print(sessKey.sesskey!)
                        if let sesskey = sessKey.sesskey {
                            UserDefaults.standard.set(sesskey, forKey: "token")
                        }
                        self.isAuthenticated = true
                        navigateToLandingPage()
                    } else {
                        loginFailed = true
                    }
                }
            }
        }
    }
}

#Preview {
    LoginView(isAuthenticated: false, token: "")
}
