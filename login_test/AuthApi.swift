//
//  AuthApi.swift
//  login_test
//
//  Created by Ivan Gustin on 3/06/24.
//

import Foundation
import UIKit

class AuthApi {
    let jokeUrl = URL(string: "https://timetonic.com/live/api.php?req=createAppkey&appname=api")!

    
    func createAppKey(completion:@escaping (AppKeyModel) -> ()) {
        var request = URLRequest(url: URL(string: "https://timetonic.com/live/api.php?req=createAppkey&appname=api")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) { data, response, error in
            let appkey = try! JSONDecoder().decode(AppKeyModel.self, from: data!)
            // print(appkey.appkey)
            DispatchQueue.main.async {
                completion(appkey)
            }
        }.resume()
    }
    
    func createOauthkey(appKey: String, email: String, pwd: String, completion:@escaping (OAuthKeyModel) -> ()) {
        var request = URLRequest(url: URL(string: "https://timetonic.com/live/api.php?req=createOauthkey&login=\(email)&pwd=\(pwd)&appkey=\(appKey)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) { data, response, error in
            let oAuthkey = try! JSONDecoder().decode(OAuthKeyModel.self, from: data!)
            // print(oAuthkey.oauthkey)
            DispatchQueue.main.async {
                completion(oAuthkey)
            }
        }.resume()
    }
    
    func createSessKey(oAuthKey: String, completion:@escaping (SessKeyModel) -> ()) {
        var request = URLRequest(url: URL(string: "https://timetonic.com/live/api.php?req=createSesskey&o_u=demo&oauthkey=\(oAuthKey)&restrictions=")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) { data, response, error in
            let sessKey = try! JSONDecoder().decode(SessKeyModel.self, from: data!)
            // print(sessKey.sesskey ?? "no key")
            DispatchQueue.main.async {
                completion(sessKey)
            }
        }.resume()
    }
    
    func getAllBooks(sesskey: String, completion:@escaping (BooksModel) -> ()) {
        var request = URLRequest(url: URL(string: "https://timetonic.com/live/api.php?req=getAllBooks&o_u=demo&u_c=demo&sesskey=\(sesskey)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) { data, response, error in
            let books = try! JSONDecoder().decode(BooksModel.self, from: data!)
            // print(String(data: data!, encoding: .utf8)!)
            DispatchQueue.main.async {
                completion(books)
            }
        }.resume()
    }
}
