//
//  oAtuhKey.swift
//  login_test
//
//  Created by Ivan Gustin on 3/06/24.
//

import Foundation

struct OAuthKeyModel: Codable, Identifiable  {
    let id: String
    let oauthkey: String
}
