//
//  LandingPageView.swift
//  login_test
//
//  Created by Ivan Gustin on 4/06/24.
//

import SwiftUI

struct LandingPageView: View {
    @State var fetchedBooks: [Book] = []
    
    var body: some View {
        VStack{
            Text("Listado de Libros")
                            .font(.title)
            List(fetchedBooks , id: \.bC) { book in
                HStack {
                    Text(book.ownerPrefs.title)
                    Spacer()
                    if let imageUrlString = book.ownerPrefs.oCoverImg,
                       let imageUrl = URL(string: "https://timetonic.com" + imageUrlString) {
        
                        AsyncImage(url: imageUrl) { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 100, height: 100)
                        } placeholder: {
                            ProgressView()
                        }
                    } else {
                        Text("Imagen no disponible")
                    }
                }
            }
    }.padding()
            .onAppear {
                getAllBooks()
            }
        }
    
    func getAllBooks () {
        if let sesskey = UserDefaults.standard.value(forKey: "token") as? String {
            AuthApi().getAllBooks(sesskey: sesskey) { (books) in
                self.fetchedBooks = books.allBooks.books
                }
        } else {
        
        }

    }
}

#Preview {
    LandingPageView()
}
